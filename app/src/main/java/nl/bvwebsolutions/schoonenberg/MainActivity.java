package nl.bvwebsolutions.schoonenberg;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private LinearLayout goToRecordingScreen;
    private DBStorage DBstorage;
    public static Map<String, List> recordArray;
    private ImageView infoButton;
    private LinearLayout content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        content = findViewById(R.id.actionContainer);

        DBstorage = new DBStorage();
        DBstorage.getRecordings("02001123", new Runnable() {

            @Override
            public void run() {
                // Use results of firestore when data has been loaded.

                for (Map.Entry<String, List> entry : recordArray.entrySet()) {
                    System.out.println(entry.getValue().get(0));

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(0, 0, 0, 35);

                    LinearLayout recordingContainer = new LinearLayout(MainActivity.this);
                    recordingContainer.setOrientation(LinearLayout.HORIZONTAL);
                    recordingContainer.setPadding(35, 25, 25, 35);
                    recordingContainer.setLayoutParams(params);
                    recordingContainer.setBackgroundColor(Color.WHITE);

                    content.addView(recordingContainer);

                    LinearLayout verticalContainer = new LinearLayout(MainActivity.this);
                    verticalContainer.setOrientation(LinearLayout.VERTICAL);
                    verticalContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
                    recordingContainer.addView(verticalContainer);

                    TextView recordingTitle = new TextView(MainActivity.this);
                    recordingTitle.setText(entry.getValue().get(0).toString());
                    recordingTitle.setTextColor(Color.parseColor("#97C11F"));
                    recordingTitle.setTextSize(20f);
                    recordingTitle.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.myriadprobold));

                    verticalContainer.addView(recordingTitle);

                    TextView recordingFreq = new TextView(MainActivity.this);
                    recordingFreq.setText(entry.getValue().get(1).toString());
                    recordingFreq.setTextColor(Color.parseColor("#7a7a7a"));
                    recordingFreq.setTextSize(18f);
                    recordingFreq.setPadding(0, 10, 0,0);
                    recordingFreq.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.myriadproregular));

                    verticalContainer.addView(recordingFreq);

                    TextView recordingDate = new TextView(MainActivity.this);
                    recordingDate.setText(entry.getValue().get(2).toString());
                    recordingDate.setTextColor(Color.parseColor("#7a7a7a"));
                    recordingDate.setTextSize(18f);
                    recordingDate.setPadding(0, 10, 0,0);
                    recordingDate.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.myriadproregular));

                    verticalContainer.addView(recordingDate);

                    LinearLayout imageContainer = new LinearLayout(MainActivity.this);
                    imageContainer.setOrientation(LinearLayout.VERTICAL);
                    imageContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.0f));
                    imageContainer.setPadding(0,0,35,0);
                    imageContainer.setGravity(Gravity.CENTER);
                    recordingContainer.addView(imageContainer);

                    ImageView succesImage = new ImageView(MainActivity.this);
                    succesImage.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 0.0f));
                    succesImage.getLayoutParams().height = 120;
                    succesImage.getLayoutParams().width = 120;
                    succesImage.setImageResource(R.drawable.succesrecording);
                    imageContainer.addView(succesImage);
                }
            }
        });

        infoButton = findViewById(R.id.infoBtn);

        goToRecordingScreen = findViewById(R.id.btmActionBtn);
        goToRecordingScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StartRecordActivity.class);
                startActivity(intent);
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

        //DBstorage.readData("02001123");

    }

    public static void initRecordings(Map<String, List> recordings){
        recordArray = recordings;
    }
}
