package nl.bvwebsolutions.schoonenberg;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;

import java.io.IOException;
import java.util.UUID;


public class StartRecordActivity extends AppCompatActivity {

    // Initiate the UI elements.
    private ImageView deviceConnectedImage;
    private TextView deviceNameText;
    private TextView statusTitle;
    private TextView recordingTimeCounter;
    private TextView activityTitle;
    private ImageButton startRecordingButton;
    private ImageView infoButton;

    private boolean isDeviceConnected = false;
    private boolean isRecording = false;
    private String deviceName;
    private MusicIntentReceiver myReceiver;

    Intent serviceIntent;
    public static final String CHANNEL_ID = "ExampleServiceChannel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_record);
        myReceiver = new MusicIntentReceiver();

        // Set the UI elements to the matching XML elements.
        deviceConnectedImage = findViewById(R.id.deviceConnected);
        deviceNameText = findViewById(R.id.deviceName);
        recordingTimeCounter = findViewById(R.id.recordingTime);
        activityTitle = findViewById(R.id.ActivityTitle);
        statusTitle = findViewById(R.id.statusTitle);
        startRecordingButton = findViewById(R.id.startButton);
        infoButton = findViewById(R.id.infoBtn);

        deviceName = "Schoonenberg Ext. Mic S500 3.5MM";

        requestRecordAudioPermission();
        requestWritePermission();

        checkDeviceConnected();

        startRecordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRecording == false) {
                    startRecording();
                    isRecording = true;
                } else {
                    Toast.makeText(getApplicationContext(), "U bent al aan het opnemen.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartRecordActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        super.onResume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void checkDeviceConnected() {
        if (isDeviceConnected) {
            deviceConnectedImage.setBackgroundResource(R.drawable.succesrecording);
            deviceNameText.setText(deviceName);
            startRecordingButton.setColorFilter(null);
            startRecordingButton.setEnabled(true);
        } else {
            deviceConnectedImage.setBackgroundResource(R.drawable.notconnected);
            deviceNameText.setText("Geen externe microfoon gevonden, sluit alstublieft de microfoon aan");

            // Apply grayscale filter
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            startRecordingButton.setColorFilter(filter);
            startRecordingButton.setEnabled(false);
        }
    }

    private void startRecording() {
        if(serviceIntent == null){
            BackgroundService.activity = this;

            // if SDK > Oreo, create notification for service;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Schoonenberg Service Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );
                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }

//            // defining geoLocation class
            serviceIntent = new Intent(this, BackgroundService.class);
            serviceIntent.putExtra("inputExtra", "");
            startService(serviceIntent);
        }

        //Set text of activity to status of recording
        activityTitle.setText("Opname momenteel bezig :");

        //Set stop text and button
        RequestManager gifRequest = Glide.with(this);
        RequestBuilder gifBuild = gifRequest.load(R.drawable.opnameanimatie);
        gifBuild.into(startRecordingButton);

        //Display the recording time
        recordingTimeCounter.setVisibility(View.VISIBLE);

        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                Long time = (30 * 1000 - millisUntilFinished) / 1000;

                if (time < 10) {
                    recordingTimeCounter.setText("00:0" + time.toString());
                } else {
                    recordingTimeCounter.setText("00:" + time.toString());
                }
            }

            public void onFinish() {

            }
        }.start();

        statusTitle.setText("Wilt u de opname stoppen?");
        deviceNameText.setText("Bent u klaar met de opname? druk dan op de onderstaande knop om de opname te verzenden.");
        deviceConnectedImage.setImageResource(R.drawable.stopbutton);

        deviceConnectedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartRecordActivity.this, RecordingScreen.class);
                startActivity(intent);

                stopService(serviceIntent);
            }
        });
    }

    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        // Headset is unplugged
                        isDeviceConnected = false;
                        break;
                    case 1:
                        // Headset is plugged
                        isDeviceConnected = true;
                        break;
                    default:
                        // I have no idea what the headset state is
                        isDeviceConnected = false;
                }
                checkDeviceConnected();
            } else {
                isDeviceConnected = false;
                checkDeviceConnected();

            }
        }
    }

    private void requestRecordAudioPermission() {
        //check API version, do nothing if API version < 23!
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
                }
            }
        }
    }

    private void requestWritePermission() {
        //check API version, do nothing if API version < 23!
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                Log.d("Activity", "Granted!");
                // start
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Log.d("Activity", "Denied!");
                finish();
            }
        }
    }


}
