package nl.bvwebsolutions.schoonenberg;

import java.util.concurrent.TimeUnit;
import android.os.Handler;

import android.media.MediaPlayer;
import android.support.annotation.RawRes;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AudioPlayback {

    private RecordingScreen initializedActivity;
    private MediaPlayer playBack;
    private @RawRes int recording;
    private ProgressBar recordingProgressBar;
    private TextView recordingDuration;
    private TextView recordingTimeStamp;
    private ImageView setAudioState;

    private int recordingTime;

    public AudioPlayback(@RawRes int recording, RecordingScreen recordActivity) {
        this.recording = recording;
        this.initializedActivity = recordActivity;
        recordingProgressBar = this.initializedActivity.findViewById(R.id.recordingProgress);
        recordingDuration = this.initializedActivity.findViewById(R.id.recordingMinutes);
        recordingTimeStamp = this.initializedActivity.findViewById(R.id.recordingProgressTimeTxt);
        setAudioState = this.initializedActivity.findViewById(R.id.changeAudioState);
        initAudio();
    }

    public void initAudio() {
        playBack = MediaPlayer.create(initializedActivity, recording);
        recordingDuration.setText(convertTime(playBack.getDuration()));
        recordingTime = playBack.getDuration() / 1000;
        recordingProgressBar.setMax(recordingTime);
    }

    public String convertTime(int Duration) {
        String time = String.format("%02d.%02d",
            TimeUnit.MILLISECONDS.toMinutes(Duration),
            TimeUnit.MILLISECONDS.toSeconds(Duration) -
            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Duration))
        );
        return time;
    }

    public void changePlayState() {
        if(!playBack.isPlaying()) {
            playBack.start();
            setAudioState.setImageResource(R.drawable.pauseaudio);
            changeProgress(recordingProgressBar);
        } else {
            playBack.pause();
            setAudioState.setImageResource(R.drawable.playaudio);
        }
    }

    public void changeProgress(final ProgressBar progress) {
        final Handler mHandler = new Handler();
        initializedActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(playBack != null){
                    int mCurrentPosition = playBack.getCurrentPosition() / 1000;
                    progress.setProgress(mCurrentPosition);
                    recordingTimeStamp.setText(convertTime(playBack.getCurrentPosition()) + " : ");
                }
                mHandler.postDelayed(this, 1000);
            }
        });
    }

    public void destroyAudio() {
        playBack.stop();
    }

}
