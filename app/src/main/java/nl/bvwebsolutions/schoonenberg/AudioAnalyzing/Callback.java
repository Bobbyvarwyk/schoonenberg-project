package nl.bvwebsolutions.schoonenberg.AudioAnalyzing;

public interface Callback {
    void onBufferAvailable(byte[] buffer);
}
