package nl.bvwebsolutions.schoonenberg.AudioAnalyzing;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class AudioActivity extends AppCompatActivity {

    private DatabaseReference myRef;
    private String TAG;

    private AudioCalculator audioCalculator;
    private Handler handler;
    private Recorder recorder;

    private TextView textFrequency;
    private TextView textDecibel;
    private Button stop_button;
    private Button start_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_main);

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message");

        myRef.setValue("Hello, World!");
        readFirebase();

        start_button = findViewById(R.id.startBtn); // start button
        start_button.setOnClickListener(start_button_handle);
        stop_button = findViewById(R.id.stopBtn); // stop button
        stop_button.setOnClickListener(stop_button_handle);
        textFrequency = findViewById(R.id.frequencyTxt);
        textDecibel = findViewById(R.id.decibelTxt);*/

        recorder = new Recorder(callback);
        audioCalculator = new AudioCalculator();
        handler = new Handler(Looper.getMainLooper());

        start_button.setEnabled(false);
        stop_button.setEnabled(true);

        requestRecordAudioPermission();
    }

    private void readFirebase(){
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    /* Start Button Handler */
    private View.OnClickListener start_button_handle = new View.OnClickListener()
    {
        public void onClick(View arg0)
        {
            start_scan();
        }
    };

    /* Stop Button Handler */
    private View.OnClickListener stop_button_handle = new View.OnClickListener()
    {
        public void onClick(View v)
        {
            recorder.stop();
            start_button.setEnabled(true);
            stop_button.setEnabled(false);
        }
    };

    private void requestRecordAudioPermission() {
        //check API version, do nothing if API version < 23!
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                Log.d("Activity", "Granted!");
                start_scan(); // start the measuring automatically when application
                // start
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Log.d("Activity", "Denied!");
                finish();
            }
        }
    }

    private void start_scan() {
        recorder.start();
        start_button.setEnabled(false); // start automatically on start
        stop_button.setEnabled(true);
    }

    private Callback callback = new Callback() {

        @Override
        public void onBufferAvailable(byte[] buffer) {
            audioCalculator.setBytes(buffer);
            int amplitude = audioCalculator.getAmplitude();
            double decibel = audioCalculator.getDecibel();
            double frequency = audioCalculator.getFrequency();
            final String db = String.valueOf(decibel + " db");
            final String hz = String.valueOf(frequency + " Hz");

            handler.post(new Runnable() {
                @Override
                public void run() {
                    textFrequency.setText(hz);
                    textDecibel.setText(db);
                }
            });
        }
    };
}
