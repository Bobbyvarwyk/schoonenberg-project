package nl.bvwebsolutions.schoonenberg;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ServiceActivity extends AppCompatActivity {

    Intent serviceIntent;
    public static final String CHANNEL_ID = "ExampleServiceChannel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);


        System.out.println(serviceIntent);


        if(serviceIntent == null){
//            BackgroundService.activity = this;

            // if SDK > Oreo, create notification for service;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Schoonenberg Service Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );
                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }

//            // defining geoLocation class
            serviceIntent = new Intent(this, BackgroundService.class);
            serviceIntent.putExtra("inputExtra", "");

            startService(serviceIntent);
        }
    }



}
