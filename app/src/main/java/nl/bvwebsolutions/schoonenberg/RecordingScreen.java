package nl.bvwebsolutions.schoonenberg;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;

import java.util.ArrayList;
import java.util.List;


public class RecordingScreen extends AppCompatActivity implements View.OnClickListener {

    private EditText recordingTitleInput;
    private LinearLayout sendButton;
    private LinearLayout cancelButton;
    public String recordingName;
    public String timeType;
    private ImageView infoButton;

    private ImageView checkBoxDaily;
    private ImageView checkBoxWeekly;
    private ImageView checkBoxMonthly;
    private ImageView setAudioState;
    private DBStorage DBstorage;
    private AudioPlayback audioPlayback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording_screen);

        FirebaseApp.initializeApp(this);
        DBstorage = new DBStorage();
        // Call the audio player
        audioPlayback = new AudioPlayback(R.raw.dontstop,this);

        checkBoxDaily = findViewById(R.id.checkboxDaily);
        checkBoxWeekly = findViewById(R.id.checkboxWeekly);
        checkBoxMonthly = findViewById(R.id.checkboxMonthly);
        setAudioState = findViewById(R.id.changeAudioState);
        sendButton = findViewById(R.id.sendButton);
        cancelButton = findViewById(R.id.cancelButton);
        recordingTitleInput = findViewById(R.id.inputTitle);
        infoButton = findViewById(R.id.infoBtn);

        // set onClicks for radio buttons
        checkBoxDaily.setOnClickListener(this);
        checkBoxWeekly.setOnClickListener(this);
        checkBoxMonthly.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        infoButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        setAudioState.setOnClickListener(this);

        RatingBar ratingBar = findViewById(R.id.RatingBar); // initiate a rating bar
        int numberOfStars = ratingBar.getNumStars(); // get total number of stars of rating bar

        DBstorage = new DBStorage();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkboxDaily:
                timeType = "Dagelijks";
                changeImage(checkBoxDaily);
                break;
            case R.id.checkboxWeekly:
                timeType = "Wekelijks";
                changeImage(checkBoxWeekly);
                break;
            case R.id.checkboxMonthly:
                timeType = "Maandelijks";
                changeImage(checkBoxMonthly);
                break;
            case R.id.sendButton:
                if(timeType == null || recordingTitleInput.getText() == null) {
                    Toast.makeText(getApplicationContext(),"Vul eerst alle vragen in voordat u verder gaat",Toast.LENGTH_LONG).show();
                } else {
                    sendData();
                    audioPlayback.destroyAudio();
                }
                break;
            case R.id.cancelButton:
                Intent intent = new Intent(RecordingScreen.this, MainActivity.class);
                startActivity(intent);
                audioPlayback.destroyAudio();
                break;
            case R.id.infoBtn:
                Intent intentToHelp = new Intent(RecordingScreen.this, HelpActivity.class);
                startActivity(intentToHelp);
                break;
            case R.id.changeAudioState:
                audioPlayback.changePlayState();
                break;
        }
    }

    public void changeImage(ImageView checkBox) {
        if (checkBox.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.radiobuttonnotchecked).getConstantState()) {
            checkBox.setImageResource(R.drawable.radiobuttonchecked);
        } else {
            checkBox.setImageResource(R.drawable.radiobuttonnotchecked);
        }
    }

    public void sendData() {
        recordingName = recordingTitleInput.getText().toString();
        System.out.println(timeType + recordingName);

        List<List> recording = new ArrayList<>();

//        double randRecTime = Math.floor(Math.random() * (900 - 300)) + 300; // max 900 = 15 mins
//        double randRecTime = Math.floor(Math.random() * (90 - 30)) + 30;  // max 90 =

        for(int i = 0; i < 90; i++){
            List<String> recordingValue =  new ArrayList<>();

            double randDecibels = Math.floor(Math.random() * (80 - 50)) + 50;
            double randFrequency = Math.floor(Math.random() * (1000 - 750)) + 750;

            int randDecibelsInt = (int) randDecibels;
            int randFrequencyInt = (int) randFrequency;

            System.out.println();

            recordingValue.add(String.valueOf(randDecibelsInt));
            recordingValue.add(String.valueOf(randFrequencyInt));

            recording.add(recordingValue);
        }

        DBStorage.getInstance().insertRecording("02001123", recordingName, timeType);
        //DBstorage.insertRecording("02001123", recordingName, timeType, recordingValue);
        Toast.makeText(getApplicationContext(),"Uw opname is succesvol verzonden",Toast.LENGTH_LONG).show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(RecordingScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
