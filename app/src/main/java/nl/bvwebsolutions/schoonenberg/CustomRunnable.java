package nl.bvwebsolutions.schoonenberg;

import java.util.List;
import java.util.Map;

public abstract class CustomRunnable implements Runnable {
    private Map<String, List> data;

    public CustomRunnable(Map<String, List> _data){
        this.data = data;
    }

    @Override
    public void run() {
        System.out.println(data);
    }

    public abstract void run(Map<String, List> recording);
}
