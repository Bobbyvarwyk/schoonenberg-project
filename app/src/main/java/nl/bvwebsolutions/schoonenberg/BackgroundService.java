package nl.bvwebsolutions.schoonenberg;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import java.util.Random;

import nl.bvwebsolutions.schoonenberg.AudioAnalyzing.AudioCalculator;
import nl.bvwebsolutions.schoonenberg.AudioAnalyzing.Callback;
import nl.bvwebsolutions.schoonenberg.AudioAnalyzing.Recorder;

import static nl.bvwebsolutions.schoonenberg.ServiceActivity.CHANNEL_ID;

public class BackgroundService extends Service {
    public static StartRecordActivity activity;
    private AudioCalculator audioCalculator;
    private Handler handler;
    private Recorder recorder;

    String output = "";

    @Override
    public void onCreate(){
        super.onCreate();
        recorder = new Recorder(callback);
        audioCalculator = new AudioCalculator();
        handler = new Handler(Looper.getMainLooper());
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        String input = intent.getStringExtra("inputExtra");

        Intent notificationIntent = new Intent(this, StartRecordActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Schoonenberg app luistert naar uw omgeving!")
                .setContentText(input)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManager.IMPORTANCE_LOW)
                .build();
        start_scan();
        startForeground(1, notification);

        return START_NOT_STICKY;
    }

    public void onDestroy(){
        recorder.stop();
        output = output.substring(0, output.length() - 1);
        DBStorage.getInstance().setRecordingValue(output);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    private void start_scan() {
        recorder.start();
    }

    private Callback callback = new Callback() {

        @Override
        public void onBufferAvailable(byte[] buffer) {
            audioCalculator.setBytes(buffer);
            int amplitude = audioCalculator.getAmplitude();
            double decibel = audioCalculator.getDecibel();
            double frequency = audioCalculator.getFrequency();
            final String db = String.valueOf(decibel + " db");
            final String hz = String.valueOf(frequency);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    Random random = new Random();
                    int db = random.nextInt(80) + 20;
                    //{db}+{hq}-{dz}+{hz}
                    output += db + "+" + hz + "-";
                }
            });
        }
    };
}
