package nl.bvwebsolutions.schoonenberg;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBStorage {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private static DBStorage instance = null;

    protected DBStorage() {
        // Exists only to defeat instantiation.
    }

    public static DBStorage getInstance() {
        if (instance == null) {
            instance = new DBStorage();
        }
        return instance;
    }

    private String recordingValue = "";

    enum DBKeys {description, situationFrequency, recording}

    public void getRecordings(String ID, final Runnable runnable){
        DocumentReference docRef = db.collection("users").document(ID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Map<String, List> recordings = new HashMap<>();
                        for(int i = 0; i < document.getData().size(); i++){

                            JSONObject c = new JSONObject(document.getData());
                            List<String> recordingString = new ArrayList<>();

                            try {
                                JSONArray recordingsJsonArray = c.getJSONArray(document.getData().keySet().toArray()[i].toString());

                                for(int x = 0; x < recordingsJsonArray.length(); x++){
                                    recordingString.add(recordingsJsonArray.getString(x));
                                }
                                recordings.put(document.getData().keySet().toArray()[i].toString(), recordingString);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        for (Map.Entry<String, List> entry : recordings.entrySet()) {
                            System.out.println(entry.getKey() + "/" + entry.getValue().get(0));
                        }

                        MainActivity.initRecordings(recordings);

                        runnable.run();

                    } else {
                        System.out.println("No such document");
                    }
                } else {
                    System.out.println("get failed with " + task.getException());
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void insertRecording(String ID, String description, String frequency) {

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd-MM-YYYY");
        Date GetDate = new Date();
        String DateStr = timeStampFormat.format(GetDate);

        List<String> valueList = new ArrayList<>();
        valueList.add(description);
        valueList.add(frequency);
        valueList.add(DateStr);
        valueList.add(recordingValue);

        RandomString randomstring = new RandomString(5);
        Map<String, Object> testMap = new HashMap<>();
        testMap.put(randomstring.nextString(), valueList);

        // Add a new document with a generated ID
        db.collection("users").document(ID)
            .set(testMap, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    System.out.println("Succsess");
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    System.out.println(e);
                }
            });
    }

    public void setRecordingValue(String recordingValue) {
        this.recordingValue = recordingValue;
    }
}
